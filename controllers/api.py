#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
API router
"""

#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import requests
from flask import Blueprint, request, Response, jsonify

#RQ
from rq import Queue
from rqWorker import conn

q = Queue(connection=conn)

apiRoute = Blueprint('api', __name__,  template_folder='views')

DATALAKE_HOST = "http://cehum.ilch.uminho.pt/ridecare/api/"

@apiRoute.route('/api/datalake/sensor1', methods=['GET', 'POST'])
def datalakeSensor1():
    if request.method == "GET":
        #return jsonify("https://cehum.ilch.uminho.pt/ridecare/json.load")
        pass
    elif request.method == "POST":
        #request.json / request.data / request.get_json(force=True)
        q.enqueue(sensor1, request.json)
        #request.form['name'] ---> form["sensor"] --> sensor 1 or 2
        #res = requests.post(DATALAKE_HOST + "sensor1", json=dataDict)
        #print('response from server:'+res.text)
        #dictFromResponse = res.json()
        #print(dictFromResponse)
    else:
        return Response(status=404)

@apiRoute.route('/api/datalake/sensor2', methods=['GET', 'POST'])
def datalakeSensor2():
    if request.method == "GET":
        #return jsonify("https://cehum.ilch.uminho.pt/ridecare/json.load/bme680")
        pass
    elif request.method == "POST":
        q.enqueue(sensor2, request.json)
    else:
        return Response(status=404)

def sensor1(dataDict):
    res = requests.post(DATALAKE_HOST + "sensor1", json=dataDict)

def sensor2(dataDict):
    res = requests.post(DATALAKE_HOST + "sensor2", json=dataDict)

#@apiRoute.route('/api/dashboard', methods=['GET', 'POST'])
#def dashboard():
#    if request.method == "GET":
#        pass
#    if request.method == "POST":
#        pass

#@apiRoute.route('/api/raspberry', methods=['GET', 'POST'])
#def raspberry():
#    if request.method == "GET":
#        pass
#    if request.method == "POST":
#        pass

#@apiRoute.route('/api/update', methods=['GET', 'POST'])
#def update():
#    if request.method == "GET":
#        pass
#    if request.method == "POST":
#        pass

#@apiRoute.route('/api/sensor1/<string:action>')
#def sensor1Action(action=None):
#    pass
